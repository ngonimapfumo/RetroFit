package ngoni.nm.xy.retrofit.pojo;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface APIInterface {

    @GET("/api/unkown")
    Call<MultipleResource>doGetListResources();

    @POST("/api/users")
    Call<User> createUser(@Body User user);

    @GET("/api/users?")
    Call<UserList> doGetUserList(@Query("page")String page);

    @FormUrlEncoded
    @POST("")
    Call<UserList> doCreateUserWithField(@Field("name")String name, @Field("job") String job);
}
